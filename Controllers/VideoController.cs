﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GzucmAspBigJob.Controllers
{
    public class VideoController : Controller
    {
        [HttpPost]
        public ActionResult UploadImg()
        {
            string filePath = "";
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase f = Request.Files["file"];
                Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
                filePath = AppDomain.CurrentDomain.BaseDirectory + @"Video\" + f.FileName;
                f.SaveAs(filePath);
            }
            return Content(filePath);
        }
    }
}