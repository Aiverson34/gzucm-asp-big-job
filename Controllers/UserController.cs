﻿using GzucmAspBigJob.Models;
using GzucmAspBigJob.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GzucmAspBigJob.Controllers
{
    public class UserController : Controller
    {

        //这个是处理接口
        [HttpPost]
        public ActionResult UploadImage()
        {
            if(Request.Files.Count > 0)
            {
                HttpPostedFileBase img = Request.Files["head_photo"];
                string fileType = Path.GetExtension(img.FileName);//获取扩展名
                string serverPath = Server.MapPath("~/Images/");
                //看看文件夹是否存在
                if (!Directory.Exists(serverPath))
                {
                    Directory.CreateDirectory(serverPath);
                }
                if (fileType == null) return Content("请检查上传的文件是否正常");
                fileType = fileType.ToLower();
                if ("(.gif)|(.jpg)|(.bmp)|(.jpeg)|(.png)".Contains(fileType))
                {
                    //生成一个随机值 避免命名冲突；
                    string fileName = System.Guid.NewGuid().ToString();
                    fileName += fileType;
                    img.SaveAs(serverPath + fileName);
                    //获取当前usr 并将头像URL存入数据库
                    var usr = UserMapper.QueryByUserAccount(Session["account"].ToString());
                    usr.PhotoUrl = "../Images/" + fileName;
                    UserMapper.UpdateUser(usr);
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    return Content("只支持上传图片");
                }
            }
            return Content("上传的图片不能为空");
        }

        // GET: User
        public ActionResult Index()
        {
            //获取当前登录用户的账号
            string account = Session["account"].ToString();
            var usr = UserMapper.QueryByUserAccount(account);
            ViewBag.CurUser = usr;
            return View();
        }

        [AllowAnonymous]
        public ActionResult ToRegisterPage()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult queryAll()
        {
            using (UserContext db = new UserContext())
            {
                var list = db.UserModels.ToList();
                string ret = "size=>" + list.Count() + "\n";
                foreach (var item in list)
                {
                    ret += item.ToString();
                    ret += "\n";
                }
                return Content(ret);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(User user)
        {
            if (ValidChecker.IsValid(user).IsVaild)
            {
                if(UserMapper.QueryByUserAccount(user.Account) != null)
                {
                    return Content("该用户名已存在!");
                }
                UserMapper.InsertUser(user);
                return RedirectToAction("ToLoginPage", "User");
            }
            return Content("注册信息不合法!");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "User");
        }

        [AllowAnonymous]
        public ActionResult ToLoginPage()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string account,string pwd)
        {
            var user = UserMapper.QueryByUserAccount(account);
            if(user == null || user.PassWord != pwd)
            {
                return RedirectToAction("ToLoginPage", "User");
            }
            Session["account"] = user.Account;
            return RedirectToAction("Index", "User");
        }
    }
}