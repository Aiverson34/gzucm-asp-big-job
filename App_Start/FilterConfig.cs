﻿using GzucmAspBigJob.Filter;
using System.Web;
using System.Web.Mvc;

namespace GzucmAspBigJob
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LoginAuthorizeAttribute());
        }
    }
}
