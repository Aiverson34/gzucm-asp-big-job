﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GzucmAspBigJob.Filter
{
    /// <summary>
    /// 判断是否有权限的过滤器
    /// </summary>
    public class LoginAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //判断是否跳过授权过滤器
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
               || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                return;
            }

            //判断登录情况
            if (filterContext.HttpContext.Session["account"] == null || filterContext.HttpContext.Session["account"].ToString() == "")
            {
                filterContext.Result = new RedirectResult("/User/ToLoginPage");
            }
        }
    }
}