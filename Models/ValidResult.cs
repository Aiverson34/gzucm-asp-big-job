﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GzucmAspBigJob.Models
{
    public class ValidResult
    {
        public List<ErrorMember> ErrorMemberList { get; set; }
        public bool IsVaild { get; set; }
    }
    public class ErrorMember
    {
        public string ErrorMessage { get; set; }
        public string ErrorMemberName { get; set; }
    }
}