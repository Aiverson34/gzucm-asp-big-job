﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;

namespace GzucmAspBigJob.Models
{
    public class PostContext : DbContext
    {
        //您的上下文已配置为从您的应用程序的配置文件(App.config 或 Web.config)
        //使用“Post”连接字符串。默认情况下，此连接字符串针对您的 LocalDb 实例上的
        //“GzucmAspBigJob.Models.Post”数据库。
        // 
        //如果您想要针对其他数据库和/或数据库提供程序，请在应用程序配置文件中修改“Post”
        //连接字符串。
        public DbSet<Post> PostModels { get; set; }

        //为您要在模型中包含的每种实体类型都添加 DbSet。有关配置和使用 Code First  模型
        //的详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=390109。

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    public class Post
    {
        [Key]
        public int PostId { get; set; }
        [Required(ErrorMessage ="标题不能为空")]
        public string Title { get; set; }
        [Required(ErrorMessage ="内容不能为空")]
        public string Context { get; set; }
        public DateTime Post_time { get; set; }
        public int UserId { get; set; }
    }

    public class PostMapper
    {
        public static List<Post> QueryByUserId(int UserId)
        {
            if (UserId == 0) return null;
            using(PostContext db=new PostContext())
            {
                List<Post> postsList = db.PostModels.Where(p => p.UserId == UserId).ToList();
                return postsList;
            }
        }
        public static string AddPost(Post post)
        {
            if (post == null) return null;
            using (PostContext db = new PostContext())
            {
                db.PostModels.Add(post);
                db.SaveChanges();
                return "success";
            }
        }

        public static string UpdatePost(Post post)
        {
            if (post.PostId == 0) return "PostId is null";
            if (post.Post_time == null) return "Post_time is null";
            if (post.UserId == 0) return "UserId is null";
            using (PostContext db = new PostContext())
            {
                Post postByPostId = db.PostModels.Find(post.PostId);
                if (postByPostId != null)
                {
                    db.Entry(post).State = EntityState.Modified;
                    db.SaveChanges();
                    return "success";
                }
                else
                {
                    return "post is null";
                }
            }
        }

        public static string Delete(int PostId)
        {
            if (PostId == 0) return null;
            using (PostContext db = new PostContext())
            {
                Post postByPostId = db.PostModels.Find(PostId);
                if (postByPostId != null)
                {
                    db.PostModels.Remove(postByPostId);
                    db.SaveChanges();
                    return "success";
                }
                else
                {
                    return "post is null";
                }
            }
        }
    }
}